
Drupal.adlib = {};

/**
 * Attach the adlib behaviour.
 */
Drupal.adlib.attach = function () {
  var settings = Drupal.settings.adlib;

  var target = $('<div />').addClass('adlib-message');
  $('.adlib-form:not(.adlib-form-processed)')
    .addClass('adlib-form-processed')
    .before($('<div />').addClass('adlib-errors'))
    .after($('<div />').addClass('adlib-preview'))
    .find('.adlib-element')
    .after(target)
    .each(function () {
      var adlibElement = this;
      var id = Drupal.adlib.getId(this);
      $(this)
        .append($('#' + id + '-wrapper').hide())
        .find('.adlib-message')
        .click(function () {
          var adlibMessage = this;
          var submit = $('<button type="submit" class="adlib-button adlib-submit" />')
            .html(settings.submit)
            .click(function () {
              $(this).addClass('throbbing');
              // We need preview functionality to be able to use Ajax.
              // Determine if there is a preview submit button.
              if ($('input.form-submit[value=' + settings.preview + ']', this.form).size()) {
                Drupal.adlib.dispatch(this, id, adlibMessage, adlibElement, target, settings);
              }
              // Otherwise we just insert the value entered in the form.
              // However, we still need Ajax since the value may have
              // been loaded from the server and is unfiltered.
              else {
                Drupal.adlib.checkPlain(this, id, adlibMessage, adlibElement, target, settings);
              }
              return false;
            });
          var cancel = $('<button type="cancel" class="adlib-button adlib-cancel" />')
            .html(settings.cancel)
            .click(function () {;
              $('#' + id + '-wrapper').hide();
              $(adlibElement)
                .find('.adlib-buttons')
                .remove();
              $(adlibMessage).fadeIn();
              return false;
            });
          var buttons = $('<span class="adlib-buttons" />')
            .append(submit)
            .append(cancel);
          $('#' + id + '-wrapper').fadeIn();
          $(this)
            .hide()
            .parent()
            .append(buttons);
        });
    });
};

/**
 * Get the ID of the form element to be used.
 */
Drupal.adlib.getId = function (elt) {
  var id = $(elt).attr('id');
  // Remove the leading adlib-.
  return id.substring(6);
};

/**
 * Submit a form for preview and extract the result for a given form
 * element.
 */
Drupal.adlib.dispatch = function (submit, id, adlibMessage, adlibElement, target, settings) {
  var form = submit.form;

  // Set options.
  var options = {
    dataType: 'json',
    data: {op: settings.preview, ajaxsubmit: 1},
    beforeSubmit: function () {
      // Remove any error messages.
      $(':input', form).removeClass('error');
      $('.adlib-errors', form).html('');
      $(adlibElement)
        .find('.adlib-submit')
        .addClass('throbbing');
    },
    success: function (data) {
      var name = $('#' + id).attr('name') || $(adlibElement).find(':input:first').attr('name');
      if (name.indexOf('[') != -1) {
        name = name.substring(0, name.indexOf('['));
      }
      if (data.errors) {
        for (var eltName in data.errors) {
          $('[@name=' + eltName + ']', form).addClass('error');
        }
        var offset = $('.adlib-errors', form).html(data.message).offset();
        $('html,body').animate({scrollTop: (offset.top - 10)}, 500);
      }
      // Set preview.
      else if (data.preview) {
        $('#' + id + '-wrapper').hide();
        var html = $(form)
          .find('.adlib-preview')
          .hide()
          .html(data.preview)
          .fadeIn()
          .find('.adlib-property-' + name)
          .html();
        $(adlibMessage).html(html);
        $(adlibElement)
          .find('.adlib-buttons')
          .remove();
        $(adlibMessage).fadeIn();
      }
    },
    error: function(data, status) {
      alert(settins.error);
      $(adlibElement)
        .find('.adlib-buttons')
        .remove();
      $(adlibMessage).fadeIn();
    },
  };

  $(form).ajaxSubmit(options);
}

/**
 * Pass the edited value of an adlib element for filtering on the server.
 */
Drupal.adlib.checkPlain = function (submit, id, adlibMessage, adlibElement, target, settings) {
  // The element may have more than one input. We just concatenate
  // the values of all. Not perfect.
  var val = $('#' + id + '-wrapper :input').fieldValue().split(' ');

  // Set options.
  var options = {
    url: settings.checkPlainPath,
    dataType: 'json',
    data: {val: val},
    beforeSubmit: function () {
      $(adlibElement)
        .find('.adlib-submit')
        .addClass('throbbing');
    },
    success: function (data) {
      $('#' + id + '-wrapper').hide();
      $(adlibMessage).html(data.val);
      $(adlibElement)
        .find('.adlib-buttons')
        .remove();
      $(adlibMessage).fadeIn();
    },
    error: function(data, status) {
      alert(settings.error);
      $(adlibElement)
        .find('.adlib-buttons')
        .remove();
      $(adlibMessage).fadeIn();
    },
  };

  $.ajax(options);
}

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.adlib.attach);
}
